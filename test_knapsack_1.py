from knapsack_1 import knapsack_0_1, example_items, example_capacity

def test_fail_example():
    assert False, "This test fails arbitrarily!"

def test_pass_example():
    print("this test passes (because it doesn't fail)")

def test_solve_0_1_knapsack():
    """
    0-1 Knapsack problem:
    Optimal value, assuming 1 of each item is available...
    """
    optimal_item_labels, optimal_value = knapsack_0_1(example_capacity, example_items)
    assert set(optimal_item_labels) == set( ["B", "C", "D", "E"] )
    assert optimal_value == 15
