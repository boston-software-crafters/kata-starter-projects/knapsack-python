from dataclasses import dataclass

@dataclass
class Item:
    label: str
    weight: int
    value: int

example_items = [
    Item("A", weight=12, value=4),
    Item("B", weight=4, value=10),
    Item("C", weight=2, value=2),
    Item("D", weight=1, value=1),
    Item("E", weight=1, value=2),
]

example_capacity = 15


def knapsack_0_1(weight_capacity, item_list):
    """
    Return the optimal list of items (with the most value)
    that fits within the weight capacity of a knapsack,
    assuming that 1 of each item is available...
    """

    # This is definitely not a good implementation!
    optimal_item_labels = []
    optimal_value = 0

    return optimal_item_labels, optimal_value
