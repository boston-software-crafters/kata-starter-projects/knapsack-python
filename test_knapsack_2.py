from knapsack_2 import knapsack_unbounded, example_items, example_capacity

# def test_solve_0_1_knapsack():
#     """
#     0-1 Knapsack problem:
#     Optimal value, assuming 1 of each item is available...
#     """
#     optimal_label_counts, optimal_value = knapsack_unbounded(example_capacity, example_items)
#     assert optimal_label_counts == { "B": 3, "E": 3 }
#     assert optimal_value == 36
